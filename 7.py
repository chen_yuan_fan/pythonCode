def findMax(array):
    max1 = array[0]
    j = 0
    for i in range(len(array)):
        if max1 <= array[i]:
            max1 = array[i]
            j = i
    del array[j]
    return array


def showArray(array):
    str1 = ""
    for i in array:
        str1 = str1 + ", " + str(i)
    return str1[1:]


def findMin(array):
    min1 = array[0]
    j = 0
    for i in range(len(array)):
        if min1 >=array[i]:
            min1 = array[i]
            j = i
    del array[j]
    return array


if __name__ == '__main__':
    avg = 0
    lst_score = [9, 10, 8, 9, 10, 7, 6, 8, 7, 8]
    print("原成绩为："+"["+showArray(lst_score)+"]")
    print("去掉最高分之后："+"["+showArray(findMax(lst_score))+"]")
    print("去掉最低分之后："+"["+showArray(findMin(lst_score))+"]")
    for i in range(len(lst_score)):
        avg = lst_score[i]+avg
    avg = avg/len(lst_score)
    print("平均成绩为："+str(avg))
